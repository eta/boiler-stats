use anyhow::{anyhow, Context};
use byteorder::{BigEndian, ReadBytesExt};
use futures_util::{StreamExt, TryFutureExt};
use image::{ImageBuffer, Rgb};
use log::{debug, error, info, trace, warn};
use openh264::decoder::{Decoder, DecoderConfig};
use retina::client::{PlayOptions, Session, SessionOptions, SetupOptions};
use retina::codec::{CodecItem, ParametersRef};
use std::fs::File;
use std::io::{Cursor, Read, Seek, SeekFrom};
use std::time::{Duration, Instant};
use tokio::sync::mpsc;
use tokio::sync::mpsc::error::TrySendError;

mod config;
mod probe;

use crate::config::Configuration;

pub(crate) type DecodedImage = ImageBuffer<Rgb<u8>, Vec<u8>>;

fn print_usage() {
    let our_name = std::env::args()
        .next()
        .unwrap_or_else(|| "boiler-stats".to_owned());
    eprintln!("usage: {} [configuration file path]", our_name);
}

fn load_config(path: &str) -> anyhow::Result<Configuration> {
    let mut file = File::open(path).context(format!(
        "failed to open config file at {}; check -h for usage",
        path
    ))?;
    let mut buffer = Vec::new();

    file.read_to_end(&mut buffer)
        .context("failed to read config")?;

    let ret = toml::from_slice(&buffer).context("failed to deserialize config")?;

    Ok(ret)
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut config_path = "./config.toml".to_owned();
    match std::env::args().nth(1).as_ref().map(|x| x as &str) {
        Some("-h") => {
            print_usage();
            return Ok(());
        }
        Some(path) => {
            config_path = path.to_owned(); // eww
        }
        None => {}
    }
    let config = load_config(&config_path)?;

    pretty_env_logger::formatted_builder()
        .filter_level(config.loglevel)
        .init();

    info!("boiler-stats v{}", env!("CARGO_PKG_VERSION"));

    let (tx, rx) = mpsc::channel(10);
    let client = reqwest::Client::new();
    let prober = probe::Prober {
        config: config.clone(),
        rx,
        reqwest: client,
    };
    tokio::spawn(prober.run().map_err(|e| {
        error!("Probe loop failed: {}", e);
        e
    }));

    info!("connecting to RTSP stream at {}", config.rtsp_uri);

    let uri = config.rtsp_uri.parse().context("parsing RTSP URI")?;
    debug!("doing DESCRIBE");

    let mut session = Session::describe(
        uri,
        SessionOptions::default().user_agent("boiler-stats/0.1".to_owned()),
    )
    .await
    .context("rtsp describe failed")?;

    let mut stream_idx = None;
    let mut video_params = None;
    for (i, stream) in session.streams().iter().enumerate() {
        debug!(
            "stream {}: media {} with encoding {} (parameters {})",
            i,
            stream.media(),
            stream.encoding_name(),
            if stream.parameters().is_some() {
                "present"
            } else {
                "absent"
            }
        );
        if stream.media() == "video" && stream.encoding_name() == "h264" {
            stream_idx = Some(i);
            if let Some(ParametersRef::Video(vp)) = stream.parameters() {
                video_params = Some(vp.clone());
            }
        }
    }
    let stream_idx = stream_idx.ok_or_else(|| anyhow!("could not find video stream"))?;
    session
        .setup(stream_idx, SetupOptions::default())
        .await
        .context("rtsp setup failed")?;
    let playing = session
        .play(PlayOptions::default())
        .await
        .context("rtsp play failed")?;
    let mut demuxed = playing.demuxed().context("rtsp demux failed")?;
    debug!("rtsp succeeded; starting openh264");
    let mut decoder =
        Decoder::with_config(DecoderConfig::default()).context("init openh264 decoder")?;
    let mut provided_headers = false;
    let mut imaged = false;
    let mut last_success = Instant::now();

    info!("streaming frames");
    while let Some(ret) = demuxed.next().await {
        let ret = match ret {
            Ok(v) => v,
            Err(e) => {
                eprintln!("[+] frame error: {}", e);
                continue;
            }
        };
        if let CodecItem::VideoFrame(vf) = ret {
            let mut cursor = Cursor::new(vf.data());

            while cursor.position() < vf.data().len() as u64 {
                let length = cursor.read_u32::<BigEndian>().unwrap();
                let mut buf = vec![0u8; length as usize + 3];
                buf[2] = 1;
                cursor.read_exact(&mut buf[3..]).unwrap();

                // We might not get the SPS and PPS NALs (headers needed to decode) in-band.
                // So we check whether they're being provided, and if they're not, we get them
                // from the SDP extra data.
                let nal_type = buf[0] & 0x1f;
                // 7 = SPS, 8 = PPS
                if nal_type == 7 && nal_type == 8 {
                    provided_headers = true;
                } else if !provided_headers {
                    warn!("detected no in-band SPS/PPS NALs, passing them manually");
                    provided_headers = true;
                    let video_params = video_params
                        .take()
                        .ok_or_else(|| anyhow!("no in-band or out-of-band headers found!"))?;
                    let mut extra_data_cursor = Cursor::new(video_params.extra_data());
                    extra_data_cursor
                        .seek(SeekFrom::Current(6))
                        .context("bad extra data (initial seek failed)")?;
                    let sps_nal_length = extra_data_cursor
                        .read_u16::<BigEndian>()
                        .context("failed to read SPS NAL length")?;
                    let mut sps_nal = vec![0u8; sps_nal_length as usize + 3];
                    sps_nal[2] = 1;
                    extra_data_cursor
                        .read_exact(&mut sps_nal[3..])
                        .context("failed to read SPS NAL")?;
                    extra_data_cursor
                        .seek(SeekFrom::Current(1))
                        .context("bad extra data (+1 seek failed)")?;

                    let pps_nal_length = extra_data_cursor
                        .read_u16::<BigEndian>()
                        .context("failed to read PPS NAL length")?;
                    let mut pps_nal = vec![0u8; pps_nal_length as usize + 3];
                    pps_nal[2] = 1;
                    extra_data_cursor
                        .read_exact(&mut pps_nal[3..])
                        .context("failed to read PPS NAL")?;
                    debug!(
                        "[+] SPS NAL length {}, PPS NAL length {}",
                        sps_nal.len(),
                        pps_nal.len()
                    );

                    decoder
                        .decode(&sps_nal)
                        .context("failed to decode SPS NAL")?;
                    decoder
                        .decode(&pps_nal)
                        .context("failed to decode PPS NAL")?;
                    debug!("passed SPS and PPS NALs, continuing");
                }

                let decode_result = decoder.decode(&buf);
                match decode_result {
                    Ok(Some(v)) => {
                        last_success = Instant::now();
                        let (width, height) = v.dimension_rgb();
                        let mut buf = vec![0u8; width * height * 3];
                        v.write_rgb8(&mut buf);
                        let ibuf =
                            ImageBuffer::<Rgb<u8>, _>::from_vec(width as u32, height as u32, buf)
                                .unwrap();
                        trace!("frame at T+{:.02}s", vf.timestamp().elapsed_secs());
                        if !imaged {
                            if let Some(ip) = config.image_path.as_ref() {
                                ibuf.save(ip)?;
                                info!("saved first decoded frame to {}", ip);
                            } else {
                                info!("got first decoded frame");
                            }
                            imaged = true;
                        }
                        match tx.try_send(ibuf) {
                            Err(TrySendError::Closed(_)) => {
                                return Err(anyhow!("probe loop failed"));
                            }
                            Err(TrySendError::Full(_)) => trace!("probe loop behind!"),
                            _ => {}
                        }
                    }
                    Ok(None) => {
                        last_success = Instant::now();
                    }
                    Err(e) => {
                        trace!("decode error: {}", e);
                        if Instant::now() - last_success > Duration::from_secs(10) {
                            error!("giving up on decoding");
                            return Err(anyhow!(
                                "failed to decode after 10 seconds; last error: {}",
                                e
                            ));
                        }
                    }
                }
            }
        }
    }

    Ok(())
}
