//! Configuration for the program and its probes.

use serde::Deserialize;

/// A probe: a request to export the brightness of a given pixel as an entity
/// in Home Assistant.
#[derive(Deserialize, Debug, Clone)]
pub struct Probe {
    /// The X coordinate of the pixel to sample.
    pub(crate) x: u32,
    /// The Y coordinate of the pixel to sample.
    pub(crate) y: u32,
    /// A unique ID for this probe.
    pub(crate) unique_id: String,
    /// A Home Assistant icon for this probe.
    pub(crate) icon: String,
    /// A Home Assistant device class for this probe.
    pub(crate) device_class: String,
    /// A Home Assistant friendly name for this probe.
    pub(crate) friendly_name: String,
}

fn default_retrans_interval() -> u64 {
    60
}

fn default_log_level() -> log::LevelFilter {
    log::LevelFilter::Info
}

/// Configuration for the program.
#[derive(Deserialize, Debug, Clone)]
pub struct Configuration {
    /// RTSP URI to subscribe to.
    pub(crate) rtsp_uri: String,
    /// URI where the Home Assistant API can be reached.
    pub(crate) home_assistant_uri: String,
    /// Bearer token for Home Assistant.
    pub(crate) home_assistant_token: String,
    /// Set of probes.
    pub(crate) probes: Vec<Probe>,
    /// The luminance threshold (Y value) to class as "on".
    pub(crate) on_luma: u8,
    /// Log level to use.
    #[serde(default = "default_log_level")]
    pub(crate) loglevel: log::LevelFilter,
    /// Path to save the first decoded image to.
    #[serde(default)]
    pub(crate) image_path: Option<String>,
    /// Time between retransmits of all state.
    #[serde(default = "default_retrans_interval")]
    pub(crate) state_retrans_interval_sec: u64,
}
